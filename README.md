# Mysql master-slave docker testing environment

## Requirements

Please make sure you have installed `docker`, `docker-compose` before following steps.

## Run the mysql master and slave

```
docker-compose up
```

Then mysql master will listen on `localhost:33065` and slave will listen on `localhost:33066`.

## Connect slave to master

Connect to your mysql slave and execute the SQL.

```SQL
CHANGE MASTER TO
    MASTER_HOST='mysql-master',
    MASTER_USER='root',
    MASTER_PASSWORD='root',
    MASTER_LOG_FILE='replicas-mysql-bin.000003',
    MASTER_LOG_POS=154;
```

## Restart slave

Connect to your mysql slave and execute the SQL.

```
stop slave;
start slave;
```